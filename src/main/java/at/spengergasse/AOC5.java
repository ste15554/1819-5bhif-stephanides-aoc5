package at.spengergasse;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class AOC5 {

    private String input;

    private void reactPolymer(ArrayList<Character> polymer) {
        boolean removed = true;
        while (removed) {
            removed = false;
            for (int i = polymer.size() - 1; i > 1; i--) {
                char char1 = polymer.get(i);
                char char0 = polymer.get(i - 1);
                if (char1 != char0) {
                    char upper1 = Character.toUpperCase(char1);
                    char upper0 = Character.toUpperCase(char0);
                    if (upper1 == upper0) {
                        polymer.remove(i);
                        polymer.remove(i - 1);
                        removed = true;
                        i--;
                    }
                }
            }
        }
    }

    public void run() throws IOException {
        Path filePath = new File("input.txt").toPath();
        Charset charset = Charset.defaultCharset();
        this.input = Files.readAllLines(filePath, charset).get(0);

        long startOne = System.currentTimeMillis();
        int resultOne = part1();
        long startTwo = System.currentTimeMillis();
        int resultTwo = part2();

        System.out.println("PART1: " + resultOne + " - Took: " + (System.currentTimeMillis() - startOne) + " ms");
        System.out.println("PART2: " + resultTwo + " - Took: " + (System.currentTimeMillis() - startTwo) + " ms");
    }

    public int part1() {
        ArrayList<Character> polymer = new ArrayList<>(this.input.length());
        for (int i = 0; i < this.input.length(); i++) {
            polymer.add(this.input.charAt(i));
        }

        reactPolymer(polymer);
        return polymer.size();
    }

    public int part2() {
        ArrayList<Character> polymer = new ArrayList<>(this.input.length());
        for (int i = 0; i < this.input.length(); i++) {
            polymer.add(this.input.charAt(i));
        }

        Integer min = null;
        for (char c = 'a'; c <= 'z'; c++) {
            char upper = Character.toUpperCase(c);
            ArrayList<Character> poly = new ArrayList<>(polymer);
            for (int i = poly.size() - 1; i > 0; i--) {
                char p = poly.get(i);
                if (p == c || p == upper) {
                    poly.remove(i);
                }
            }
            reactPolymer(poly);
            int size = poly.size();
            if (min == null || size < min) {
                min = size;
            }
        }

        return min;
    }

    public static void main(String[] args) throws IOException {
        new AOC5().run();
    }

}
